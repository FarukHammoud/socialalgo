var mouseX,mouseY;
function init()
{
    canvas = document.getElementById("canvas");
    canvas.width = document.body.clientWidth; //document.width is obsolete
    canvas.height = document.body.clientHeight; //document.height is obsolete
    canvasW = canvas.width;
    canvasH = canvas.height;

    if( canvas.getContext )
    {
        setup();
        setInterval( run , 33 );
    }
}
document.onmousemove = function (e) {mousePos(e);};
function mousePos(e){
    mouseX = e.clientX;
    mouseY = e.clientY;
    console.log(mouseX,mouseY)
}
function c_trivial(data,i){
    return 'rgb('+data[0]*255+','+data[1]*255+','+data[2]*255+')';
}
function array_gen(l = 10){
    var arr = [];
    for(var i=0;i<l;i++){
        arr.push(Math.random());
    }
    return arr;
}
function average(data_1,data_2){
    var data = [data_1[0]+(data_2[0]-data_1[0])*0.01,data_1[1]+(data_2[1]-data_1[1])*0.01,data_1[2]+(data_2[2]-data_1[2])*0.01] 
    return data
}
class Data{
    constructor(data,color_function){
        this.data = data;
        this.color_function = color_function;
    }
    color(i){
        return this.color_function(this.data,i);
    }
    static distance(data_1,data_2){
        return Math.min(Math.abs(data_1.data[0]-data_2.data[0]),Math.abs(data_1.data[1]-data_2.data[1]),Math.abs(data_1.data[2]-data_2.data[2]))
    }
    static operation(data_1,data_2){
        return average(data_1,data_2);
    }
}
class Agent{
    constructor(pos_x,pos_y,p_radius = 1,data){
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.p_radius = p_radius;
        this.data = data;
    }
    draw(i){
        var color = this.data.color(i);
        circle(this.pos_x,this.pos_y,10,true,true,color,'grey');
        circle(this.pos_x,this.pos_y,this.p_radius,false,true,color,'grey');
    }

}
class Society{
    constructor(n_agents,p=0.5){
        this.agents = [];
        this.iterations = 0;
        this.p = p;
        for(var i=0;i<n_agents;i++){
            var pos_x = Math.random()*window.innerWidth;
            var pos_y = Math.random()*window.innerHeight;
            var p_radius = Math.random()*window.innerWidth*0.3;
            var data = new Data(array_gen(3),c_trivial);
            this.agents.push(new Agent(pos_x,pos_y,p_radius,data));
        }
    }
    getIterations(){
        return this.iterations;
    }
    communication(i,j){
        var u12 = {x : this.agents[j].pos_x - this.agents[i].pos_x,
                   y : this.agents[j].pos_y - this.agents[i].pos_y }
        var n = Math.sqrt(Math.pow(u12.x,2)+Math.pow(u12.y,2),0.5);
        var nu12 = {x : u12.x/n,y : u12.y/n}
        var av_p = {x : (this.agents[j].pos_x + this.agents[i].pos_x)/2,
                    y : (this.agents[j].pos_y + this.agents[i].pos_y)/2 }
        var d = Data.distance(this.agents[i].data,this.agents[j].data)
        var k = Math.max(10,n/2+(d-0.1)*0.3)
        this.agents[i].pos_x = av_p.x-k*nu12.x;
        this.agents[j].pos_x = av_p.x+k*nu12.x;

        this.agents[i].pos_y = av_p.y-k*nu12.y;
        this.agents[j].pos_y = av_p.y+k*nu12.y;
        this.agents[i].data.data = Data.operation(this.agents[i].data.data,this.agents[j].data.data);
    }
    possible(i){
        var possible = [];
        for (var j=0;j<this.agents.length;j++){
            if(j != i){
                var d2 = Math.pow(this.agents[i].pos_x-this.agents[j].pos_x,2)+Math.pow(this.agents[i].pos_y-this.agents[j].pos_y,2);
                if ( d2 < Math.pow(this.agents[i].p_radius,2) ){
                    possible.push(j);
                }
            }
        }
        return possible;
    }
    run(){
        this.iterations++;
        for (var i=0;i<this.agents.length;i++){
            var possible = this.possible(i);
            for (var j of possible){
                if (Math.random()>= this.p){
                    this.communication(i,j);
                }
            }
            this.agents[i].p_radius += (-this.agents[i].p_radius+20*(possible.length+1))*0.001 ; 
            if(this.agents[i].p_radius < 10){
                this.agents[i].p_radius = 10;
            }
        }
    }
    draw(){
        clear()
        for(var i=0;i<this.agents.length;i++){
            this.agents[i].draw(this.iterations%10);
        }
    }

}
function clear(){
    var canvas = document.getElementById('canvas');
    if (canvas.getContext){
        var ctx = canvas.getContext('2d'); 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}
function circle(x,y,r,fill = true,stroke = true,c1 = 'white',c2 = 'white'){
    var canvas = document.getElementById('canvas');
    if (canvas.getContext){
        var ctx = canvas.getContext('2d'); 
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI);
        if(fill){
            ctx.fillStyle = c1;
            ctx.fill();
        }
        if(stroke){
            ctx.lineWidth = 1;
            ctx.strokeStyle = c2;
            ctx.stroke();
        }
    }
}
society = new Society(20);

var id = setInterval(iteration,10)

function iteration(){
    if (society.getIterations() == 10000){
        clearInterval(id);
    }
    else{
        society.run();
        society.draw();
        //console.log(society.getIterations());
    }
}


